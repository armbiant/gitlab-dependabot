#!/bin/bash

set -eu

source "$(dirname "$0")/utils.sh"

release_image="registry.gitlab.com/dependabot-gitlab/dependabot:latest"

log "** Pulling docker images **"
export BASE_IMAGE="$APP_IMAGE"
docker compose pull --quiet --include-deps 2>/dev/null
docker pull --quiet "$release_image"

log "** Initializing database from latest release **"
BASE_IMAGE="$release_image" docker compose run --rm migration 2>/dev/null
BASE_IMAGE="$release_image" docker compose run --rm migration bundle exec rake "db:seed" 2>/dev/null

log "** Running migrations **"
docker compose run --rm migration 2>&1

log "** Validate no migrations are pending **"
docker compose run --rm migration bundle exec rake "dependabot:check_migrations" 2>/dev/null

log "** Running seed data **"
docker compose run --rm migration bundle exec rake "db:seed" 2>/dev/null
