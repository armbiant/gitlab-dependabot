# frozen_string_literal: true

class UpdaterConfig < ApplicationConfig
  COMPOSE_DEPLOYMENT = "compose"
  K8S_DEPLOYMENT = "k8s"

  env_prefix :settings_

  attr_config :updater_image_pattern,
              :deploy_mode,
              updater_template_path: nil,
              delete_updater_container: true,
              # job configuration
              update_retry: 2,
              expire_run_data: 1.month.to_i,
              updater_container_startup_timeout: 180

  # Deployed via docker compose
  #
  # @return [Boolean]
  def compose_deployment?
    deploy_mode == COMPOSE_DEPLOYMENT
  end

  # Deployment via k8s
  #
  # @return [Boolean]
  def k8s_deployment?
    deploy_mode == K8S_DEPLOYMENT
  end

  # Amount of tries for update job
  #
  # @return [Numeric, Boolean]
  def job_retries
    update_retry.is_a?(Numeric) ? update_retry : update_retry == "true"
  end

  # App deployment mode
  #
  # @return [String]
  def deploy_mode
    mode = super
    raise("Deploy mode must be set via 'SETTINGS__DEPLOY_MODE' env variable!") unless mode
    raise(<<~ERR.strip) unless [COMPOSE_DEPLOYMENT, K8S_DEPLOYMENT].include?(mode)
      Unsupported deploy mode, must be set to '#{COMPOSE_DEPLOYMENT}' or '#{K8S_DEPLOYMENT}' via 'SETTINGS__DEPLOY_MODE' environment variable
    ERR

    mode
  end

  # Updater template
  #
  # @return [String]
  def updater_template_path
    return super if super
    return "docker-compose.yml" if compose_deployment?
    raise("Updater template must be set via 'SETTINGS__UPDATER_TEMPLATE_PATH'") if k8s_deployment?
  end
end
