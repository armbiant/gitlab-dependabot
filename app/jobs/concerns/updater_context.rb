# frozen_string_literal: true

module UpdaterContext
  extend ActiveSupport::Concern

  # :reek:LongParameterList

  # Updater job details
  #
  # @param [String] job
  # @param [String] project_name
  # @param [String] ecosystem
  # @param [String] directory
  # @return [Hash]
  def job_details(job, project_name, ecosystem, directory)
    directory = directory == "/" ? nil : directory if directory

    { job: job, project_name: project_name, ecosystem: ecosystem, directory: directory }.compact
  end
end
