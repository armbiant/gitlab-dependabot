# frozen_string_literal: true

class ProjectRegistrationJob < ApplicationJob
  queue_as :project_registration
  sidekiq_options retry: 0

  def perform
    log(:info, "Checking for projects to register or update", tags: ["project-registration"])
    Dependabot::Projects::Registration::Service.call
  end
end
