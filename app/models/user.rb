# frozen_string_literal: true

require "active_record/secure_token"

class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  include ActiveRecord::SecureToken

  field :username, type: String
  field :password_digest, type: String
  field :remember_token, type: String

  has_secure_password
  has_secure_token :remember_token

  validates :username, presence: true, uniqueness: true

  index({ remember_token: 1 }, { unique: true })
end
