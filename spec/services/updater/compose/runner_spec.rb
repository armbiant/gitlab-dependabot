# frozen_string_literal: true

describe Updater::Compose::Runner do
  subject(:runner) { described_class }

  let(:project) { "test-project" }
  let(:ecosystem) { "bundler" }
  let(:directory) { "/" }
  let(:sha) { "123" }

  let(:stdin) { instance_double(IO, write: nil, close: nil) }
  let(:err) { instance_double(IO, read: "command error output") }
  let(:wait_thr) { instance_double(Process::Waiter, value: instance_double(Process::Status, success?: success)) }
  let(:out) { ["command output"] }

  let(:base_image) { "image:tag" }

  let(:compose_hash) do
    {
      "version" => "3.8",
      "x-updater" => {
        "updater-conf" => "conf"
      }
    }
  end

  let(:compose_cmd) do
    [
      "docker", "compose", "-f", "-", "run", "--no-TTY", "--quiet-pull",
      "--name", "updater-#{ecosystem}-#{sha}-#{sha}", "--rm", "updater",
      "bundle", "exec", "rake", "dependabot:update[#{project},#{ecosystem},#{directory}]"
    ]
  end

  let(:env) do
    {
      "BASE_IMAGE" => base_image,
      "SETTINGS__DEPLOY_MODE" => "compose",
      "SETTINGS__UPDATER_IMAGE_PATTERN" => "image-%<package_ecosystem>s:latest"
    }
  end

  before do
    allow(Open3).to receive(:popen3).and_yield(stdin, out, err, wait_thr)
    allow(YAML).to receive(:load_file).with("docker-compose.yml", aliases: true).and_return(compose_hash)
    allow(SecureRandom).to receive(:alphanumeric).and_return(sha)
  end

  around do |example|
    with_env(env) { example.run }
  end

  context "with success" do
    let(:success) { true }

    it "executes updater compose command and prints output" do
      expect { runner.call(project, ecosystem, directory) }.to output("#{out.first}\n").to_stdout

      expect(Open3).to have_received(:popen3).with(*compose_cmd)
      expect(stdin).to have_received(:close)
      expect(stdin).to have_received(:write).with(YAML.dump({
        "version" => "3.8",
        "services" => {
          "updater" => {
            "image" => "image-bundler:latest",
            **compose_hash["x-updater"]
          }
        }
      }))
    end
  end

  context "with failure" do
    let(:success) { false }

    before do
      allow($stdout).to receive(:puts)
    end

    it "raises UpdaterFailure error with stderr output" do
      expect { runner.call(project, ecosystem, directory) }.to raise_error(
        Updater::UpdaterFailure,
        "command error output"
      )
    end
  end
end
